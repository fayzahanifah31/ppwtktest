from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

class beritatest(TestCase):

    def test_home_urls_exist(self):
        response = Client().get('/berita/')
        self.assertEqual(response.status_code, 200)

    def test_home_render_right(self):
        response= Client().get('/berita/')
        html_response = response.content.decode('utf8')
        self.assertIn('Berita', html_response)
        self.assertIn('card', html_response)   