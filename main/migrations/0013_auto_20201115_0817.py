# Generated by Django 3.1.3 on 2020-11-15 01:17

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0012_auto_20201115_0813'),
    ]

    operations = [
        migrations.AlterField(
            model_name='saran',
            name='tanggal',
            field=models.DateField(blank=True, default=datetime.datetime(2020, 11, 15, 1, 17, 6, 959093, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
