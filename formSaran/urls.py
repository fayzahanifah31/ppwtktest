from django.urls import path

from . import views

app_name = 'formSaran'

urlpatterns = [
    path('', views.saran, name='saran'),
    path('create/', views.create, name='create'),
    path('list1/', views.list1, name='list1'),
]