from main.models import Saran
from django.shortcuts import render,redirect


from .forms import PostForm

# Create your views here.
def saran(request):
    context = {
        'page_title':'Home',

    }
    return render(request,'saran.html',context)

def list1(request):
    posts = Saran.objects.all()

    context = {
        'page_title':'Semua Post',
        'posts':posts,
    }
    print(posts)
    return render(request,'formSaran/list1.html',context)

def create(request):
    post_form = PostForm(request.POST or None)

    if request.method == 'POST' :#post request dari browser
        if post_form.is_valid():
            post_form.save()

            return redirect('formSaran:list1')

    context = {
        'page_title':'Create Post',
        'post_form': post_form
    }
    return render(request,'formSaran/create.html',context)

